var CalendarDispatcher = require('../Dispatch/CalendarDispatcher');
var DB = require('../Data/CalendarDB');
var Constants = require('./Constants');
var pouchCollate = require('pouchdb-collate');

var CalendarActionCreator = {
	saveCalendarEvent: function(document){
		var self = this;
		
		CalendarDispatcher.dispatch({eventName: Constants.SAVE_BEGIN, document: document});
		
		if (!document.hasOwnProperty("_id")){
			var d = new Date();
			document._id = pouchCollate.toIndexableString([d, document.name]);
			document.key = d.toJSON() + '/' + document.name;
		}
		
		DB.put(document).then(
			function(result) {
				CalendarDispatcher.dispatch({eventName: Constants.SAVE_SUCCESS, document: document, result: result});
				self.fetchAllCalendarEvents();
			},
			function(err) {
				CalendarDispatcher.dispatch({eventName: Constants.SAVE_FAILURE, document: document, error: err});
			});
	},
	deleteCalendarEvent: function(document){
		var self = this;
		
		CalendarDispatcher.dispatch({eventName: Constants.DELETE_BEGIN, document: document});
		
		DB.remove(document).then(
			function(result) {
				CalendarDispatcher.dispatch({eventName: Constants.DELETE_SUCCESS, document: document, result: result});
				self.fetchAllCalendarEvents();
			},
			function(err) {
				CalendarDispatcher.dispatch({eventName: Constants.DELETE_FAILURE, document: document, error: err});
			});
	},
	fetchAllCalendarEvents: function(){
		CalendarDispatcher.dispatch({eventName: Constants.FETCH_BEGIN});
		
		DB.allDocs({include_docs: true}).then(
			function(result) {
				CalendarDispatcher.dispatch({eventName: Constants.FETCH_SUCCESS, documents: result.rows});
			},
			function(err) {
				CalendarDispatcher.dispatch({eventName: Constants.FETCH_FAILURE, error: err});
			});
	}
};

module.exports = CalendarActionCreator;