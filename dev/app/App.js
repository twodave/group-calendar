/** @jsx React.DOM */
var React = require('react');

// components
var EventList = require('./Components/EventList');
var EventForm = require('./Components/EventForm.js');

var CalendarActionCreator = require('./Actions/CalendarActionCreator');
CalendarActionCreator.fetchAllCalendarEvents();

var App = React.createClass({

	render: function () {
		
		return (
				
				<div className="container">
					<h1>Group Calendar</h1>
					<EventList />
					<EventForm />
				</div>
			)
	}

});

module.exports = App;