/** @jsx React.DOM */
var React = require('react');
var CalendarStore = require('../Stores/CalendarStore');
var Immutable = require('immutable');
var CalendarActionCreator = require('../Actions/CalendarActionCreator');
var CalendarEvent = require('./CalendarEvent');

var EventList = React.createClass({
	componentDidMount: function(){
		CalendarStore.bind('change', this.calendarChanged);
	},
	componentWillUnmount: function(){
		CalendarStore.unbind('change', this.calendarChanged);
	},
	render: function () {
		var empty = <li>No events yet.</li>
		var rows = this.state.calendarEvents.map(function(result) {
							return <CalendarEvent key={result.get('key')} data={result.toObject()} />;
						}).toArray();
		if (rows.length == 0){
			rows = empty;
		}
		return (
				<div>
					<h2>Event List</h2>
					<ul>
						{rows}
					</ul>
				</div>
			);
	},
	getInitialState: function() {
		return {calendarEvents: new Immutable.List([])};
	},
	calendarChanged: function(evt){
		this.setState({calendarEvents: CalendarStore.documents});
	}

});

module.exports = EventList;