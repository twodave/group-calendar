/** @jsx React.DOM */
var React = require('react');
var CalendarActionCreator = require('../Actions/CalendarActionCreator');

var EventForm = React.createClass({
	componentDidMount: function(){
		this.refs.name.getDOMNode().focus();
	},
	componentWillUnmount: function(){
	},
	render: function () {
		return (
				<form className="well" onSubmit={this.handleSubmit}>
					<h2>New Event</h2>
					<div className="form-group">
						<label htmlFor="name">Title</label>
						<input 
							className="form-control"
							ref="name" 
							type="text" 
							id="name"
							value={this.state.document.name}
							onChange={this.handleNameChange} />
					</div>
					<button type="submit" className="btn btn-default">Submit</button>
				</form>
			);
	},
	getInitialState: function() {
		return {
			document:{
				name: ''
			}
		};
	},
	handleNameChange: function(e) {
		this.setState({
			document: {
				name: e.target.value
			}
		});
	},
	handleSubmit: function(e) {
		e.preventDefault();
		var document = this.state.document;
		if (document.name.trim()){
			CalendarActionCreator.saveCalendarEvent(document);
			this.setState({
				document:{
					name: ''
				}
			});
			this.refs.name.getDOMNode().focus();
		}
	}
});

module.exports = EventForm;