/** @jsx React.DOM */
var React = require('react');
var Immutable = require('immutable');
var CalendarActionCreator = require('../Actions/CalendarActionCreator');

var CalendarEvent = React.createClass({
	componentDidMount: function(){
	},
	componentWillUnmount: function(){
	},
	render: function () {
		return (
				<li>{this.props.data.name} <a onClick={this.handleDelete} style={{'text-decoration':'underline',cursor:'pointer'}}>Delete</a></li>
			);
	},
	getInitialState: function() {
		return {};
	},
	handleDelete: function(e){
		CalendarActionCreator.deleteCalendarEvent(this.props.data);
	}
});

module.exports = CalendarEvent;