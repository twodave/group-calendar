var $ = require('jquery');
var MicroEvent = require('microevent');
var Immutable = require('immutable');
var Constants = require('../Actions/Constants');
var dispatcher = require('../Dispatch/CalendarDispatcher');

var PouchStore = function () {
    var self = this;
    
    self.documents = new Immutable.List();
    
    dispatcher.register(function(payload){
        switch(payload.eventName) {
			case Constants.FETCH_SUCCESS:
				self.documents = new Immutable.List(payload.documents.map(function(row){
					return new Immutable.Map(row.doc);
				}));
				
				self.trigger('change');
        }
        return true;
    });
};

MicroEvent.mixin(PouchStore);

module.exports = PouchStore;