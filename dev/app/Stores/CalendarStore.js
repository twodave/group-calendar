var PouchStore = require('./PouchStore');
var PouchDB = require('pouchdb');
var CalendarDispatcher = require('../Dispatch/CalendarDispatcher');

var db = new PouchDB('GroupCalendar')

var CalendarStore = new PouchStore(db, CalendarDispatcher);

module.exports = CalendarStore;