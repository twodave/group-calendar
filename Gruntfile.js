module.exports = function (grunt) {

	var webpack = require('webpack');
	/*
	 * SOME OPTIONS VARIABLES
	 */

	// Our two bundles
	var entry = {
		main: ['./dev/app/main.js'], // Application bundle
		vendors: ['bootstrap/dist/css/bootstrap.css', 'jquery', 'bootstrap', 'react', 'react-dispatcher', 'immutable', 'pouchdb', 'pouchdb-collate', 'microevent'] // Vendor bundle
	};

	// Creates a special Commons bundle that our application can require from
	var commonPlugin = new webpack.optimize.CommonsChunkPlugin("vendors", "vendors.js");

	// We need to uglify that code on deploy
	var uglifyPlugin = new webpack.optimize.UglifyJsPlugin();

	// The loader transforms our JSX content
	var module = {
		loaders: [
			// **IMPORTANT** This is needed so that each bootstrap js file required by
			// bootstrap-webpack has access to the jQuery object
			//{ test: require.resolve("jquery"), loader: "" },
			
			{ test: /\.js$/, loader: 'imports?jQuery=jquery!jsx' },
			{ test: /\.css$/, loader: 'style-loader!css-loader' },
		
			// Needed for the css-loader when [bootstrap-webpack](https://github.com/bline/bootstrap-webpack)
			// loads bootstrap's css.
			{ test: /\.woff$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
			{ test: /\.woff2$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
			{ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream" },
			{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
			{ test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=image/svg+xml" }
		]
	};

	grunt.initConfig({
		webpack: {
			dev: {
				entry: entry,
				plugins: [commonPlugin],
				watch: true,
				keepalive: true,
				stats: {
					timings: true
				},
				devtool: "#inline-source-map", // Here we get our sourcemap
				output: {
					filename: 'main.js',
					path: './build'
				},
				module: module
			},
			dist: {
				entry: entry,
				plugins: [commonPlugin, uglifyPlugin],
				output: {
					filename: 'main.js',
					path: './dist'
				},
				module: module
			}
		}
	});

	grunt.loadNpmTasks('grunt-webpack');

	grunt.registerTask('build', ['webpack:dev']);

	grunt.registerTask('deploy', ['webpack:dist']);

}