webpackJsonp([0],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },

/***/ 1:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	/** @jsx React.DOM */
	var React = __webpack_require__(3);
	var App = __webpack_require__(148);
	var bootstrap = __webpack_require__(271);
	React.renderComponent(App(null), document.body);


/***/ },

/***/ 148:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	/** @jsx React.DOM */
	var React = __webpack_require__(3);
	
	// components
	var EventList = __webpack_require__(149);
	var EventForm = __webpack_require__(270);
	
	var CalendarActionCreator = __webpack_require__(265);
	CalendarActionCreator.fetchAllCalendarEvents();
	
	var App = React.createClass({displayName: 'App',
	
		render: function () {
			
			return (
					
					React.DOM.div({className: "container"}, 
						React.DOM.h1(null, "Group Calendar"), 
						EventList(null), 
						EventForm(null)
					)
				)
		}
	
	});
	
	module.exports = App;


/***/ },

/***/ 149:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	/** @jsx React.DOM */
	var React = __webpack_require__(3);
	var CalendarStore = __webpack_require__(150);
	var Immutable = __webpack_require__(154);
	var CalendarActionCreator = __webpack_require__(265);
	var CalendarEvent = __webpack_require__(269);
	
	var EventList = React.createClass({displayName: 'EventList',
		componentDidMount: function(){
			CalendarStore.bind('change', this.calendarChanged);
		},
		componentWillUnmount: function(){
			CalendarStore.unbind('change', this.calendarChanged);
		},
		render: function () {
			var empty = React.DOM.li(null, "No events yet.")
			var rows = this.state.calendarEvents.map(function(result) {
								return CalendarEvent({key: result.get('key'), data: result.toObject()});
							}).toArray();
			if (rows.length == 0){
				rows = empty;
			}
			return (
					React.DOM.div(null, 
						React.DOM.h2(null, "Event List"), 
						React.DOM.ul(null, 
							rows
						)
					)
				);
		},
		getInitialState: function() {
			return {calendarEvents: new Immutable.List([])};
		},
		calendarChanged: function(evt){
			this.setState({calendarEvents: CalendarStore.documents});
		}
	
	});
	
	module.exports = EventList;


/***/ },

/***/ 150:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	var PouchStore = __webpack_require__(151);
	var PouchDB = __webpack_require__(160);
	var CalendarDispatcher = __webpack_require__(156);
	
	var db = new PouchDB('GroupCalendar')
	
	var CalendarStore = new PouchStore(db, CalendarDispatcher);
	
	module.exports = CalendarStore;


/***/ },

/***/ 151:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	var $ = __webpack_require__(2);
	var MicroEvent = __webpack_require__(152);
	var Immutable = __webpack_require__(154);
	var Constants = __webpack_require__(155);
	var dispatcher = __webpack_require__(156);
	
	var PouchStore = function () {
	    var self = this;
	    
	    self.documents = new Immutable.List();
	    
	    dispatcher.register(function(payload){
	        switch(payload.eventName) {
				case Constants.FETCH_SUCCESS:
					self.documents = new Immutable.List(payload.documents.map(function(row){
						return new Immutable.Map(row.doc);
					}));
					
					self.trigger('change');
	        }
	        return true;
	    });
	};
	
	MicroEvent.mixin(PouchStore);
	
	module.exports = PouchStore;


/***/ },

/***/ 155:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	module.exports = {
		SAVE_BEGIN:'SAVE_BEGIN',
		SAVE_SUCCESS: 'SAVE_SUCCESS',
		SAVE_FAILURE: 'SAVE_FAILURE',
		FETCH_BEGIN:'FETCH_BEGIN',
		FETCH_SUCCESS: 'FETCH_SUCCESS',
		FETCH_FAILURE: 'FETCH_FAILURE',
		DELETE_BEGIN:'DELETE_BEGIN',
		DELETE_SUCCESS: 'DELETE_SUCCESS',
		DELETE_FAILURE: 'DELETE_FAILURE'
	};


/***/ },

/***/ 156:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	var Dispatcher = __webpack_require__(157);
	
	var CalendarDispatcher = new Dispatcher();
	
	module.exports = CalendarDispatcher;


/***/ },

/***/ 265:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	var CalendarDispatcher = __webpack_require__(156);
	var DB = __webpack_require__(266);
	var Constants = __webpack_require__(155);
	var pouchCollate = __webpack_require__(267);
	
	var CalendarActionCreator = {
		saveCalendarEvent: function(document){
			var self = this;
			
			CalendarDispatcher.dispatch({eventName: Constants.SAVE_BEGIN, document: document});
			
			if (!document.hasOwnProperty("_id")){
				var d = new Date();
				document._id = pouchCollate.toIndexableString([d, document.name]);
				document.key = d.toJSON() + '/' + document.name;
			}
			
			DB.put(document).then(
				function(result) {
					CalendarDispatcher.dispatch({eventName: Constants.SAVE_SUCCESS, document: document, result: result});
					self.fetchAllCalendarEvents();
				},
				function(err) {
					CalendarDispatcher.dispatch({eventName: Constants.SAVE_FAILURE, document: document, error: err});
				});
		},
		deleteCalendarEvent: function(document){
			var self = this;
			
			CalendarDispatcher.dispatch({eventName: Constants.DELETE_BEGIN, document: document});
			
			DB.remove(document).then(
				function(result) {
					CalendarDispatcher.dispatch({eventName: Constants.DELETE_SUCCESS, document: document, result: result});
					self.fetchAllCalendarEvents();
				},
				function(err) {
					CalendarDispatcher.dispatch({eventName: Constants.DELETE_FAILURE, document: document, error: err});
				});
		},
		fetchAllCalendarEvents: function(){
			CalendarDispatcher.dispatch({eventName: Constants.FETCH_BEGIN});
			
			DB.allDocs({include_docs: true}).then(
				function(result) {
					CalendarDispatcher.dispatch({eventName: Constants.FETCH_SUCCESS, documents: result.rows});
				},
				function(err) {
					CalendarDispatcher.dispatch({eventName: Constants.FETCH_FAILURE, error: err});
				});
		}
	};
	
	module.exports = CalendarActionCreator;


/***/ },

/***/ 266:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	var PouchDB = __webpack_require__(160);
	var db = new PouchDB('GroupCalendar');
	
	module.exports = db;


/***/ },

/***/ 269:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	/** @jsx React.DOM */
	var React = __webpack_require__(3);
	var Immutable = __webpack_require__(154);
	var CalendarActionCreator = __webpack_require__(265);
	
	var CalendarEvent = React.createClass({displayName: 'CalendarEvent',
		componentDidMount: function(){
		},
		componentWillUnmount: function(){
		},
		render: function () {
			return (
					React.DOM.li(null, this.props.data.name, " ", React.DOM.a({onClick: this.handleDelete, style: {'text-decoration':'underline',cursor:'pointer'}}, "Delete"))
				);
		},
		getInitialState: function() {
			return {};
		},
		handleDelete: function(e){
			CalendarActionCreator.deleteCalendarEvent(this.props.data);
		}
	});
	
	module.exports = CalendarEvent;


/***/ },

/***/ 270:
/***/ function(module, exports, __webpack_require__) {

	/*** IMPORTS FROM imports-loader ***/
	var jQuery = __webpack_require__(2);
	
	/** @jsx React.DOM */
	var React = __webpack_require__(3);
	var CalendarActionCreator = __webpack_require__(265);
	
	var EventForm = React.createClass({displayName: 'EventForm',
		componentDidMount: function(){
			this.refs.name.getDOMNode().focus();
		},
		componentWillUnmount: function(){
		},
		render: function () {
			return (
					React.DOM.form({className: "well", onSubmit: this.handleSubmit}, 
						React.DOM.h2(null, "New Event"), 
						React.DOM.div({className: "form-group"}, 
							React.DOM.label({htmlFor: "name"}, "Title"), 
							React.DOM.input({
								className: "form-control", 
								ref: "name", 
								type: "text", 
								id: "name", 
								value: this.state.document.name, 
								onChange: this.handleNameChange})
						), 
						React.DOM.button({type: "submit", className: "btn btn-default"}, "Submit")
					)
				);
		},
		getInitialState: function() {
			return {
				document:{
					name: ''
				}
			};
		},
		handleNameChange: function(e) {
			this.setState({
				document: {
					name: e.target.value
				}
			});
		},
		handleSubmit: function(e) {
			e.preventDefault();
			var document = this.state.document;
			if (document.name.trim()){
				CalendarActionCreator.saveCalendarEvent(document);
				this.setState({
					document:{
						name: ''
					}
				});
				this.refs.name.getDOMNode().focus();
			}
		}
	});
	
	module.exports = EventForm;


/***/ }

});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9kZXYvYXBwL21haW4uanMiLCJ3ZWJwYWNrOi8vLy4vZGV2L2FwcC9BcHAuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2L2FwcC9Db21wb25lbnRzL0V2ZW50TGlzdC5qcyIsIndlYnBhY2s6Ly8vLi9kZXYvYXBwL1N0b3Jlcy9DYWxlbmRhclN0b3JlLmpzIiwid2VicGFjazovLy8uL2Rldi9hcHAvU3RvcmVzL1BvdWNoU3RvcmUuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2L2FwcC9BY3Rpb25zL0NvbnN0YW50cy5qcyIsIndlYnBhY2s6Ly8vLi9kZXYvYXBwL0Rpc3BhdGNoL0NhbGVuZGFyRGlzcGF0Y2hlci5qcyIsIndlYnBhY2s6Ly8vLi9kZXYvYXBwL0FjdGlvbnMvQ2FsZW5kYXJBY3Rpb25DcmVhdG9yLmpzIiwid2VicGFjazovLy8uL2Rldi9hcHAvRGF0YS9DYWxlbmRhckRCLmpzIiwid2VicGFjazovLy8uL2Rldi9hcHAvQ29tcG9uZW50cy9DYWxlbmRhckV2ZW50LmpzIiwid2VicGFjazovLy8uL2Rldi9hcHAvQ29tcG9uZW50cy9FdmVudEZvcm0uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNQQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsOEJBQTZCOztBQUU3Qjs7QUFFQTs7QUFFQSxvQkFBbUIsdUJBQXVCO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxFQUFDOztBQUVEOzs7Ozs7OztBQzdCQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxvQ0FBbUM7QUFDbkM7QUFDQTtBQUNBLEdBQUU7QUFDRjtBQUNBO0FBQ0EsR0FBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBLDhCQUE2QixnREFBZ0Q7QUFDN0UsUUFBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFFO0FBQ0Y7QUFDQSxXQUFVO0FBQ1YsR0FBRTtBQUNGO0FBQ0Esa0JBQWlCLHdDQUF3QztBQUN6RDs7QUFFQSxFQUFDOztBQUVEOzs7Ozs7OztBQzNDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7QUNYQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQSxNQUFLO0FBQ0w7O0FBRUE7O0FBRUE7Ozs7Ozs7O0FDN0JBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNiQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOzs7Ozs7OztBQ1BBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGdDQUErQixvREFBb0Q7O0FBRW5GO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGtDQUFpQyxzRUFBc0U7QUFDdkc7QUFDQSxLQUFJO0FBQ0o7QUFDQSxrQ0FBaUMsa0VBQWtFO0FBQ25HLEtBQUk7QUFDSixHQUFFO0FBQ0Y7QUFDQTs7QUFFQSxnQ0FBK0Isc0RBQXNEOztBQUVyRjtBQUNBO0FBQ0Esa0NBQWlDLHdFQUF3RTtBQUN6RztBQUNBLEtBQUk7QUFDSjtBQUNBLGtDQUFpQyxvRUFBb0U7QUFDckcsS0FBSTtBQUNKLEdBQUU7QUFDRjtBQUNBLGdDQUErQixpQ0FBaUM7O0FBRWhFLGVBQWMsbUJBQW1CO0FBQ2pDO0FBQ0Esa0NBQWlDLDJEQUEyRDtBQUM1RixLQUFJO0FBQ0o7QUFDQSxrQ0FBaUMsK0NBQStDO0FBQ2hGLEtBQUk7QUFDSjtBQUNBOztBQUVBOzs7Ozs7OztBQ3hEQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FDTkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSx3Q0FBdUM7QUFDdkM7QUFDQSxHQUFFO0FBQ0Y7QUFDQSxHQUFFO0FBQ0Y7QUFDQTtBQUNBLGdFQUErRCxvQ0FBb0MsZ0RBQWdEO0FBQ25KO0FBQ0EsR0FBRTtBQUNGO0FBQ0E7QUFDQSxHQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0EsRUFBQzs7QUFFRDs7Ozs7Ozs7QUMxQkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsb0NBQW1DO0FBQ25DO0FBQ0E7QUFDQSxHQUFFO0FBQ0Y7QUFDQSxHQUFFO0FBQ0Y7QUFDQTtBQUNBLHFCQUFvQiwrQ0FBK0M7QUFDbkU7QUFDQSxxQkFBb0Isd0JBQXdCO0FBQzVDLHdCQUF1QixnQkFBZ0I7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXVDO0FBQ3ZDO0FBQ0Esd0JBQXVCLDZDQUE2QztBQUNwRTtBQUNBO0FBQ0EsR0FBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBRztBQUNILEdBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0EsRUFBQzs7QUFFRCIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKiBJTVBPUlRTIEZST00gaW1wb3J0cy1sb2FkZXIgKioqL1xudmFyIGpRdWVyeSA9IHJlcXVpcmUoXCJqcXVlcnlcIik7XG5cbi8qKiBAanN4IFJlYWN0LkRPTSAqL1xyXG52YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xyXG52YXIgQXBwID0gcmVxdWlyZSgnLi9BcHAuanMnKTtcclxudmFyIGJvb3RzdHJhcCA9IHJlcXVpcmUoJ2Jvb3RzdHJhcCcpO1xyXG5SZWFjdC5yZW5kZXJDb21wb25lbnQoQXBwKG51bGwpLCBkb2N1bWVudC5ib2R5KTtcblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9kZXYvYXBwL21haW4uanNcbiAqKiBtb2R1bGUgaWQgPSAxXG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCIvKioqIElNUE9SVFMgRlJPTSBpbXBvcnRzLWxvYWRlciAqKiovXG52YXIgalF1ZXJ5ID0gcmVxdWlyZShcImpxdWVyeVwiKTtcblxuLyoqIEBqc3ggUmVhY3QuRE9NICovXHJcbnZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XHJcblxyXG4vLyBjb21wb25lbnRzXHJcbnZhciBFdmVudExpc3QgPSByZXF1aXJlKCcuL0NvbXBvbmVudHMvRXZlbnRMaXN0Jyk7XHJcbnZhciBFdmVudEZvcm0gPSByZXF1aXJlKCcuL0NvbXBvbmVudHMvRXZlbnRGb3JtLmpzJyk7XHJcblxyXG52YXIgQ2FsZW5kYXJBY3Rpb25DcmVhdG9yID0gcmVxdWlyZSgnLi9BY3Rpb25zL0NhbGVuZGFyQWN0aW9uQ3JlYXRvcicpO1xyXG5DYWxlbmRhckFjdGlvbkNyZWF0b3IuZmV0Y2hBbGxDYWxlbmRhckV2ZW50cygpO1xyXG5cclxudmFyIEFwcCA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogJ0FwcCcsXHJcblxyXG5cdHJlbmRlcjogZnVuY3Rpb24gKCkge1xyXG5cdFx0XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdFJlYWN0LkRPTS5kaXYoe2NsYXNzTmFtZTogXCJjb250YWluZXJcIn0sIFxyXG5cdFx0XHRcdFx0UmVhY3QuRE9NLmgxKG51bGwsIFwiR3JvdXAgQ2FsZW5kYXJcIiksIFxyXG5cdFx0XHRcdFx0RXZlbnRMaXN0KG51bGwpLCBcclxuXHRcdFx0XHRcdEV2ZW50Rm9ybShudWxsKVxyXG5cdFx0XHRcdClcclxuXHRcdFx0KVxyXG5cdH1cclxuXHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBBcHA7XG5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZGV2L2FwcC9BcHAuanNcbiAqKiBtb2R1bGUgaWQgPSAxNDhcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIi8qKiogSU1QT1JUUyBGUk9NIGltcG9ydHMtbG9hZGVyICoqKi9cbnZhciBqUXVlcnkgPSByZXF1aXJlKFwianF1ZXJ5XCIpO1xuXG4vKiogQGpzeCBSZWFjdC5ET00gKi9cclxudmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcclxudmFyIENhbGVuZGFyU3RvcmUgPSByZXF1aXJlKCcuLi9TdG9yZXMvQ2FsZW5kYXJTdG9yZScpO1xyXG52YXIgSW1tdXRhYmxlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyk7XHJcbnZhciBDYWxlbmRhckFjdGlvbkNyZWF0b3IgPSByZXF1aXJlKCcuLi9BY3Rpb25zL0NhbGVuZGFyQWN0aW9uQ3JlYXRvcicpO1xyXG52YXIgQ2FsZW5kYXJFdmVudCA9IHJlcXVpcmUoJy4vQ2FsZW5kYXJFdmVudCcpO1xyXG5cclxudmFyIEV2ZW50TGlzdCA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogJ0V2ZW50TGlzdCcsXHJcblx0Y29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCl7XHJcblx0XHRDYWxlbmRhclN0b3JlLmJpbmQoJ2NoYW5nZScsIHRoaXMuY2FsZW5kYXJDaGFuZ2VkKTtcclxuXHR9LFxyXG5cdGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpe1xyXG5cdFx0Q2FsZW5kYXJTdG9yZS51bmJpbmQoJ2NoYW5nZScsIHRoaXMuY2FsZW5kYXJDaGFuZ2VkKTtcclxuXHR9LFxyXG5cdHJlbmRlcjogZnVuY3Rpb24gKCkge1xyXG5cdFx0dmFyIGVtcHR5ID0gUmVhY3QuRE9NLmxpKG51bGwsIFwiTm8gZXZlbnRzIHlldC5cIilcclxuXHRcdHZhciByb3dzID0gdGhpcy5zdGF0ZS5jYWxlbmRhckV2ZW50cy5tYXAoZnVuY3Rpb24ocmVzdWx0KSB7XHJcblx0XHRcdFx0XHRcdFx0cmV0dXJuIENhbGVuZGFyRXZlbnQoe2tleTogcmVzdWx0LmdldCgna2V5JyksIGRhdGE6IHJlc3VsdC50b09iamVjdCgpfSk7XHJcblx0XHRcdFx0XHRcdH0pLnRvQXJyYXkoKTtcclxuXHRcdGlmIChyb3dzLmxlbmd0aCA9PSAwKXtcclxuXHRcdFx0cm93cyA9IGVtcHR5O1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIChcclxuXHRcdFx0XHRSZWFjdC5ET00uZGl2KG51bGwsIFxyXG5cdFx0XHRcdFx0UmVhY3QuRE9NLmgyKG51bGwsIFwiRXZlbnQgTGlzdFwiKSwgXHJcblx0XHRcdFx0XHRSZWFjdC5ET00udWwobnVsbCwgXHJcblx0XHRcdFx0XHRcdHJvd3NcclxuXHRcdFx0XHRcdClcclxuXHRcdFx0XHQpXHJcblx0XHRcdCk7XHJcblx0fSxcclxuXHRnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG5cdFx0cmV0dXJuIHtjYWxlbmRhckV2ZW50czogbmV3IEltbXV0YWJsZS5MaXN0KFtdKX07XHJcblx0fSxcclxuXHRjYWxlbmRhckNoYW5nZWQ6IGZ1bmN0aW9uKGV2dCl7XHJcblx0XHR0aGlzLnNldFN0YXRlKHtjYWxlbmRhckV2ZW50czogQ2FsZW5kYXJTdG9yZS5kb2N1bWVudHN9KTtcclxuXHR9XHJcblxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gRXZlbnRMaXN0O1xuXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2Rldi9hcHAvQ29tcG9uZW50cy9FdmVudExpc3QuanNcbiAqKiBtb2R1bGUgaWQgPSAxNDlcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIi8qKiogSU1QT1JUUyBGUk9NIGltcG9ydHMtbG9hZGVyICoqKi9cbnZhciBqUXVlcnkgPSByZXF1aXJlKFwianF1ZXJ5XCIpO1xuXG52YXIgUG91Y2hTdG9yZSA9IHJlcXVpcmUoJy4vUG91Y2hTdG9yZScpO1xyXG52YXIgUG91Y2hEQiA9IHJlcXVpcmUoJ3BvdWNoZGInKTtcclxudmFyIENhbGVuZGFyRGlzcGF0Y2hlciA9IHJlcXVpcmUoJy4uL0Rpc3BhdGNoL0NhbGVuZGFyRGlzcGF0Y2hlcicpO1xyXG5cclxudmFyIGRiID0gbmV3IFBvdWNoREIoJ0dyb3VwQ2FsZW5kYXInKVxyXG5cclxudmFyIENhbGVuZGFyU3RvcmUgPSBuZXcgUG91Y2hTdG9yZShkYiwgQ2FsZW5kYXJEaXNwYXRjaGVyKTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQ2FsZW5kYXJTdG9yZTtcblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9kZXYvYXBwL1N0b3Jlcy9DYWxlbmRhclN0b3JlLmpzXG4gKiogbW9kdWxlIGlkID0gMTUwXG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCIvKioqIElNUE9SVFMgRlJPTSBpbXBvcnRzLWxvYWRlciAqKiovXG52YXIgalF1ZXJ5ID0gcmVxdWlyZShcImpxdWVyeVwiKTtcblxudmFyICQgPSByZXF1aXJlKCdqcXVlcnknKTtcclxudmFyIE1pY3JvRXZlbnQgPSByZXF1aXJlKCdtaWNyb2V2ZW50Jyk7XHJcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcclxudmFyIENvbnN0YW50cyA9IHJlcXVpcmUoJy4uL0FjdGlvbnMvQ29uc3RhbnRzJyk7XHJcbnZhciBkaXNwYXRjaGVyID0gcmVxdWlyZSgnLi4vRGlzcGF0Y2gvQ2FsZW5kYXJEaXNwYXRjaGVyJyk7XHJcblxyXG52YXIgUG91Y2hTdG9yZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIFxyXG4gICAgc2VsZi5kb2N1bWVudHMgPSBuZXcgSW1tdXRhYmxlLkxpc3QoKTtcclxuICAgIFxyXG4gICAgZGlzcGF0Y2hlci5yZWdpc3RlcihmdW5jdGlvbihwYXlsb2FkKXtcclxuICAgICAgICBzd2l0Y2gocGF5bG9hZC5ldmVudE5hbWUpIHtcclxuXHRcdFx0Y2FzZSBDb25zdGFudHMuRkVUQ0hfU1VDQ0VTUzpcclxuXHRcdFx0XHRzZWxmLmRvY3VtZW50cyA9IG5ldyBJbW11dGFibGUuTGlzdChwYXlsb2FkLmRvY3VtZW50cy5tYXAoZnVuY3Rpb24ocm93KXtcclxuXHRcdFx0XHRcdHJldHVybiBuZXcgSW1tdXRhYmxlLk1hcChyb3cuZG9jKTtcclxuXHRcdFx0XHR9KSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0c2VsZi50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9KTtcclxufTtcclxuXHJcbk1pY3JvRXZlbnQubWl4aW4oUG91Y2hTdG9yZSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFBvdWNoU3RvcmU7XG5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZGV2L2FwcC9TdG9yZXMvUG91Y2hTdG9yZS5qc1xuICoqIG1vZHVsZSBpZCA9IDE1MVxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiLyoqKiBJTVBPUlRTIEZST00gaW1wb3J0cy1sb2FkZXIgKioqL1xudmFyIGpRdWVyeSA9IHJlcXVpcmUoXCJqcXVlcnlcIik7XG5cbm1vZHVsZS5leHBvcnRzID0ge1xyXG5cdFNBVkVfQkVHSU46J1NBVkVfQkVHSU4nLFxyXG5cdFNBVkVfU1VDQ0VTUzogJ1NBVkVfU1VDQ0VTUycsXHJcblx0U0FWRV9GQUlMVVJFOiAnU0FWRV9GQUlMVVJFJyxcclxuXHRGRVRDSF9CRUdJTjonRkVUQ0hfQkVHSU4nLFxyXG5cdEZFVENIX1NVQ0NFU1M6ICdGRVRDSF9TVUNDRVNTJyxcclxuXHRGRVRDSF9GQUlMVVJFOiAnRkVUQ0hfRkFJTFVSRScsXHJcblx0REVMRVRFX0JFR0lOOidERUxFVEVfQkVHSU4nLFxyXG5cdERFTEVURV9TVUNDRVNTOiAnREVMRVRFX1NVQ0NFU1MnLFxyXG5cdERFTEVURV9GQUlMVVJFOiAnREVMRVRFX0ZBSUxVUkUnXHJcbn07XG5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZGV2L2FwcC9BY3Rpb25zL0NvbnN0YW50cy5qc1xuICoqIG1vZHVsZSBpZCA9IDE1NVxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiLyoqKiBJTVBPUlRTIEZST00gaW1wb3J0cy1sb2FkZXIgKioqL1xudmFyIGpRdWVyeSA9IHJlcXVpcmUoXCJqcXVlcnlcIik7XG5cbnZhciBEaXNwYXRjaGVyID0gcmVxdWlyZSgncmVhY3QtZGlzcGF0Y2hlcicpO1xyXG5cclxudmFyIENhbGVuZGFyRGlzcGF0Y2hlciA9IG5ldyBEaXNwYXRjaGVyKCk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IENhbGVuZGFyRGlzcGF0Y2hlcjtcblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9kZXYvYXBwL0Rpc3BhdGNoL0NhbGVuZGFyRGlzcGF0Y2hlci5qc1xuICoqIG1vZHVsZSBpZCA9IDE1NlxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiLyoqKiBJTVBPUlRTIEZST00gaW1wb3J0cy1sb2FkZXIgKioqL1xudmFyIGpRdWVyeSA9IHJlcXVpcmUoXCJqcXVlcnlcIik7XG5cbnZhciBDYWxlbmRhckRpc3BhdGNoZXIgPSByZXF1aXJlKCcuLi9EaXNwYXRjaC9DYWxlbmRhckRpc3BhdGNoZXInKTtcclxudmFyIERCID0gcmVxdWlyZSgnLi4vRGF0YS9DYWxlbmRhckRCJyk7XHJcbnZhciBDb25zdGFudHMgPSByZXF1aXJlKCcuL0NvbnN0YW50cycpO1xyXG52YXIgcG91Y2hDb2xsYXRlID0gcmVxdWlyZSgncG91Y2hkYi1jb2xsYXRlJyk7XHJcblxyXG52YXIgQ2FsZW5kYXJBY3Rpb25DcmVhdG9yID0ge1xyXG5cdHNhdmVDYWxlbmRhckV2ZW50OiBmdW5jdGlvbihkb2N1bWVudCl7XHJcblx0XHR2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHRcclxuXHRcdENhbGVuZGFyRGlzcGF0Y2hlci5kaXNwYXRjaCh7ZXZlbnROYW1lOiBDb25zdGFudHMuU0FWRV9CRUdJTiwgZG9jdW1lbnQ6IGRvY3VtZW50fSk7XHJcblx0XHRcclxuXHRcdGlmICghZG9jdW1lbnQuaGFzT3duUHJvcGVydHkoXCJfaWRcIikpe1xyXG5cdFx0XHR2YXIgZCA9IG5ldyBEYXRlKCk7XHJcblx0XHRcdGRvY3VtZW50Ll9pZCA9IHBvdWNoQ29sbGF0ZS50b0luZGV4YWJsZVN0cmluZyhbZCwgZG9jdW1lbnQubmFtZV0pO1xyXG5cdFx0XHRkb2N1bWVudC5rZXkgPSBkLnRvSlNPTigpICsgJy8nICsgZG9jdW1lbnQubmFtZTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0REIucHV0KGRvY3VtZW50KS50aGVuKFxyXG5cdFx0XHRmdW5jdGlvbihyZXN1bHQpIHtcclxuXHRcdFx0XHRDYWxlbmRhckRpc3BhdGNoZXIuZGlzcGF0Y2goe2V2ZW50TmFtZTogQ29uc3RhbnRzLlNBVkVfU1VDQ0VTUywgZG9jdW1lbnQ6IGRvY3VtZW50LCByZXN1bHQ6IHJlc3VsdH0pO1xyXG5cdFx0XHRcdHNlbGYuZmV0Y2hBbGxDYWxlbmRhckV2ZW50cygpO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRmdW5jdGlvbihlcnIpIHtcclxuXHRcdFx0XHRDYWxlbmRhckRpc3BhdGNoZXIuZGlzcGF0Y2goe2V2ZW50TmFtZTogQ29uc3RhbnRzLlNBVkVfRkFJTFVSRSwgZG9jdW1lbnQ6IGRvY3VtZW50LCBlcnJvcjogZXJyfSk7XHJcblx0XHRcdH0pO1xyXG5cdH0sXHJcblx0ZGVsZXRlQ2FsZW5kYXJFdmVudDogZnVuY3Rpb24oZG9jdW1lbnQpe1xyXG5cdFx0dmFyIHNlbGYgPSB0aGlzO1xyXG5cdFx0XHJcblx0XHRDYWxlbmRhckRpc3BhdGNoZXIuZGlzcGF0Y2goe2V2ZW50TmFtZTogQ29uc3RhbnRzLkRFTEVURV9CRUdJTiwgZG9jdW1lbnQ6IGRvY3VtZW50fSk7XHJcblx0XHRcclxuXHRcdERCLnJlbW92ZShkb2N1bWVudCkudGhlbihcclxuXHRcdFx0ZnVuY3Rpb24ocmVzdWx0KSB7XHJcblx0XHRcdFx0Q2FsZW5kYXJEaXNwYXRjaGVyLmRpc3BhdGNoKHtldmVudE5hbWU6IENvbnN0YW50cy5ERUxFVEVfU1VDQ0VTUywgZG9jdW1lbnQ6IGRvY3VtZW50LCByZXN1bHQ6IHJlc3VsdH0pO1xyXG5cdFx0XHRcdHNlbGYuZmV0Y2hBbGxDYWxlbmRhckV2ZW50cygpO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHRmdW5jdGlvbihlcnIpIHtcclxuXHRcdFx0XHRDYWxlbmRhckRpc3BhdGNoZXIuZGlzcGF0Y2goe2V2ZW50TmFtZTogQ29uc3RhbnRzLkRFTEVURV9GQUlMVVJFLCBkb2N1bWVudDogZG9jdW1lbnQsIGVycm9yOiBlcnJ9KTtcclxuXHRcdFx0fSk7XHJcblx0fSxcclxuXHRmZXRjaEFsbENhbGVuZGFyRXZlbnRzOiBmdW5jdGlvbigpe1xyXG5cdFx0Q2FsZW5kYXJEaXNwYXRjaGVyLmRpc3BhdGNoKHtldmVudE5hbWU6IENvbnN0YW50cy5GRVRDSF9CRUdJTn0pO1xyXG5cdFx0XHJcblx0XHREQi5hbGxEb2NzKHtpbmNsdWRlX2RvY3M6IHRydWV9KS50aGVuKFxyXG5cdFx0XHRmdW5jdGlvbihyZXN1bHQpIHtcclxuXHRcdFx0XHRDYWxlbmRhckRpc3BhdGNoZXIuZGlzcGF0Y2goe2V2ZW50TmFtZTogQ29uc3RhbnRzLkZFVENIX1NVQ0NFU1MsIGRvY3VtZW50czogcmVzdWx0LnJvd3N9KTtcclxuXHRcdFx0fSxcclxuXHRcdFx0ZnVuY3Rpb24oZXJyKSB7XHJcblx0XHRcdFx0Q2FsZW5kYXJEaXNwYXRjaGVyLmRpc3BhdGNoKHtldmVudE5hbWU6IENvbnN0YW50cy5GRVRDSF9GQUlMVVJFLCBlcnJvcjogZXJyfSk7XHJcblx0XHRcdH0pO1xyXG5cdH1cclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQ2FsZW5kYXJBY3Rpb25DcmVhdG9yO1xuXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2Rldi9hcHAvQWN0aW9ucy9DYWxlbmRhckFjdGlvbkNyZWF0b3IuanNcbiAqKiBtb2R1bGUgaWQgPSAyNjVcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIi8qKiogSU1QT1JUUyBGUk9NIGltcG9ydHMtbG9hZGVyICoqKi9cbnZhciBqUXVlcnkgPSByZXF1aXJlKFwianF1ZXJ5XCIpO1xuXG52YXIgUG91Y2hEQiA9IHJlcXVpcmUoJ3BvdWNoZGInKTtcclxudmFyIGRiID0gbmV3IFBvdWNoREIoJ0dyb3VwQ2FsZW5kYXInKTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZGI7XG5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZGV2L2FwcC9EYXRhL0NhbGVuZGFyREIuanNcbiAqKiBtb2R1bGUgaWQgPSAyNjZcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIi8qKiogSU1QT1JUUyBGUk9NIGltcG9ydHMtbG9hZGVyICoqKi9cbnZhciBqUXVlcnkgPSByZXF1aXJlKFwianF1ZXJ5XCIpO1xuXG4vKiogQGpzeCBSZWFjdC5ET00gKi9cclxudmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcclxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xyXG52YXIgQ2FsZW5kYXJBY3Rpb25DcmVhdG9yID0gcmVxdWlyZSgnLi4vQWN0aW9ucy9DYWxlbmRhckFjdGlvbkNyZWF0b3InKTtcclxuXHJcbnZhciBDYWxlbmRhckV2ZW50ID0gUmVhY3QuY3JlYXRlQ2xhc3Moe2Rpc3BsYXlOYW1lOiAnQ2FsZW5kYXJFdmVudCcsXHJcblx0Y29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCl7XHJcblx0fSxcclxuXHRjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKXtcclxuXHR9LFxyXG5cdHJlbmRlcjogZnVuY3Rpb24gKCkge1xyXG5cdFx0cmV0dXJuIChcclxuXHRcdFx0XHRSZWFjdC5ET00ubGkobnVsbCwgdGhpcy5wcm9wcy5kYXRhLm5hbWUsIFwiIFwiLCBSZWFjdC5ET00uYSh7b25DbGljazogdGhpcy5oYW5kbGVEZWxldGUsIHN0eWxlOiB7J3RleHQtZGVjb3JhdGlvbic6J3VuZGVybGluZScsY3Vyc29yOidwb2ludGVyJ319LCBcIkRlbGV0ZVwiKSlcclxuXHRcdFx0KTtcclxuXHR9LFxyXG5cdGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4ge307XHJcblx0fSxcclxuXHRoYW5kbGVEZWxldGU6IGZ1bmN0aW9uKGUpe1xyXG5cdFx0Q2FsZW5kYXJBY3Rpb25DcmVhdG9yLmRlbGV0ZUNhbGVuZGFyRXZlbnQodGhpcy5wcm9wcy5kYXRhKTtcclxuXHR9XHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBDYWxlbmRhckV2ZW50O1xuXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2Rldi9hcHAvQ29tcG9uZW50cy9DYWxlbmRhckV2ZW50LmpzXG4gKiogbW9kdWxlIGlkID0gMjY5XG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCIvKioqIElNUE9SVFMgRlJPTSBpbXBvcnRzLWxvYWRlciAqKiovXG52YXIgalF1ZXJ5ID0gcmVxdWlyZShcImpxdWVyeVwiKTtcblxuLyoqIEBqc3ggUmVhY3QuRE9NICovXHJcbnZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XHJcbnZhciBDYWxlbmRhckFjdGlvbkNyZWF0b3IgPSByZXF1aXJlKCcuLi9BY3Rpb25zL0NhbGVuZGFyQWN0aW9uQ3JlYXRvcicpO1xyXG5cclxudmFyIEV2ZW50Rm9ybSA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtkaXNwbGF5TmFtZTogJ0V2ZW50Rm9ybScsXHJcblx0Y29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCl7XHJcblx0XHR0aGlzLnJlZnMubmFtZS5nZXRET01Ob2RlKCkuZm9jdXMoKTtcclxuXHR9LFxyXG5cdGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpe1xyXG5cdH0sXHJcblx0cmVuZGVyOiBmdW5jdGlvbiAoKSB7XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFJlYWN0LkRPTS5mb3JtKHtjbGFzc05hbWU6IFwid2VsbFwiLCBvblN1Ym1pdDogdGhpcy5oYW5kbGVTdWJtaXR9LCBcclxuXHRcdFx0XHRcdFJlYWN0LkRPTS5oMihudWxsLCBcIk5ldyBFdmVudFwiKSwgXHJcblx0XHRcdFx0XHRSZWFjdC5ET00uZGl2KHtjbGFzc05hbWU6IFwiZm9ybS1ncm91cFwifSwgXHJcblx0XHRcdFx0XHRcdFJlYWN0LkRPTS5sYWJlbCh7aHRtbEZvcjogXCJuYW1lXCJ9LCBcIlRpdGxlXCIpLCBcclxuXHRcdFx0XHRcdFx0UmVhY3QuRE9NLmlucHV0KHtcclxuXHRcdFx0XHRcdFx0XHRjbGFzc05hbWU6IFwiZm9ybS1jb250cm9sXCIsIFxyXG5cdFx0XHRcdFx0XHRcdHJlZjogXCJuYW1lXCIsIFxyXG5cdFx0XHRcdFx0XHRcdHR5cGU6IFwidGV4dFwiLCBcclxuXHRcdFx0XHRcdFx0XHRpZDogXCJuYW1lXCIsIFxyXG5cdFx0XHRcdFx0XHRcdHZhbHVlOiB0aGlzLnN0YXRlLmRvY3VtZW50Lm5hbWUsIFxyXG5cdFx0XHRcdFx0XHRcdG9uQ2hhbmdlOiB0aGlzLmhhbmRsZU5hbWVDaGFuZ2V9KVxyXG5cdFx0XHRcdFx0KSwgXHJcblx0XHRcdFx0XHRSZWFjdC5ET00uYnV0dG9uKHt0eXBlOiBcInN1Ym1pdFwiLCBjbGFzc05hbWU6IFwiYnRuIGJ0bi1kZWZhdWx0XCJ9LCBcIlN1Ym1pdFwiKVxyXG5cdFx0XHRcdClcclxuXHRcdFx0KTtcclxuXHR9LFxyXG5cdGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRkb2N1bWVudDp7XHJcblx0XHRcdFx0bmFtZTogJydcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHR9LFxyXG5cdGhhbmRsZU5hbWVDaGFuZ2U6IGZ1bmN0aW9uKGUpIHtcclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRkb2N1bWVudDoge1xyXG5cdFx0XHRcdG5hbWU6IGUudGFyZ2V0LnZhbHVlXHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH0sXHJcblx0aGFuZGxlU3VibWl0OiBmdW5jdGlvbihlKSB7XHJcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHR2YXIgZG9jdW1lbnQgPSB0aGlzLnN0YXRlLmRvY3VtZW50O1xyXG5cdFx0aWYgKGRvY3VtZW50Lm5hbWUudHJpbSgpKXtcclxuXHRcdFx0Q2FsZW5kYXJBY3Rpb25DcmVhdG9yLnNhdmVDYWxlbmRhckV2ZW50KGRvY3VtZW50KTtcclxuXHRcdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdFx0ZG9jdW1lbnQ6e1xyXG5cdFx0XHRcdFx0bmFtZTogJydcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cdFx0XHR0aGlzLnJlZnMubmFtZS5nZXRET01Ob2RlKCkuZm9jdXMoKTtcclxuXHRcdH1cclxuXHR9XHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBFdmVudEZvcm07XG5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZGV2L2FwcC9Db21wb25lbnRzL0V2ZW50Rm9ybS5qc1xuICoqIG1vZHVsZSBpZCA9IDI3MFxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIl0sInNvdXJjZVJvb3QiOiIifQ==